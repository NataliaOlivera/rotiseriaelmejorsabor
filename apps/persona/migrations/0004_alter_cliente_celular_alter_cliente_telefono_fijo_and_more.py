# Generated by Django 4.1.2 on 2022-10-29 05:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('persona', '0003_alter_cliente_celular_alter_cliente_telefono_fijo_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='celular',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='telefono_fijo',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
        migrations.AlterField(
            model_name='empleado',
            name='celular',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
        migrations.AlterField(
            model_name='empleado',
            name='telefono_fijo',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
        migrations.AlterField(
            model_name='repartidor',
            name='celular',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
        migrations.AlterField(
            model_name='repartidor',
            name='telefono_fijo',
            field=models.DecimalField(blank=True, decimal_places=0, max_digits=78, null=True),
        ),
    ]

from django.urls import path

from . import views
from .views import creacion_cliente, listar_clientes, modificar_cliente, creacion_empleado, listar_empleados, eliminar_cliente, modificar_empleado, eliminar_empleado, listar_repartidores, modificar_repartidor, eliminar_repartidor

app_name = 'persona'

urlpatterns = [
    path('registrar_cliente/', creacion_cliente, name='registrar_cliente'),

    path('listar_clientes/', listar_clientes, name='listar_clientes'),

    path('modificar_cliente/<id>', modificar_cliente, name='modificar_cliente'),

    path('registrar_empleado/', views.creacion_empleado, name='registrar_empleado'),

    path('creacion_repartidor/', views.creacion_repartidor,
         name='creacion_repartidor'),

    path('listar_empleados/', listar_empleados, name='listar_empleados'),

    path('modificar_empleado/<id>', modificar_empleado, name='modificar_empleado'),


    path('eliminar_cliente/<id>', eliminar_cliente, name='eliminar_cliente'),

    path('eliminar_empleado/<id>', eliminar_empleado, name='eliminar_empleado'),



    ####################


    path('listar_repartidores/', listar_repartidores, name='listar_repartidores'),

    path('modificar_repartidor/<id>', modificar_repartidor,
         name='modificar_repartidor'),


    path('eliminar_repartidor/<id>', eliminar_repartidor,
         name='eliminar_repartidor'),



    ###############


]

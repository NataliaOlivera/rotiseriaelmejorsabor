from django.contrib import admin

from .models import Cliente,Empleado,Repartidor

admin.site.register(Cliente)

admin.site.register(Empleado)

admin.site.register(Repartidor)

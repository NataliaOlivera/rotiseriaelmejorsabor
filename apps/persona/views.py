from audioop import reverse

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages

from .forms import nuevoClienteForm, nuevoEmpleadoForm, nuevoRepartidorForm
from .models import Cliente, Empleado, Repartidor
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required, permission_required


# Create your views here.

############################## CLIENTE##############################

@permission_required('persona.view_Cliente', login_url="usuario:login")
def listar_clientes(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'persona/listar_clientes.html', {'clientes': Cliente.objects.all()})

@permission_required('persona.add_Cliente', login_url="usuario:login")
def creacion_cliente(request):
    if request.user.is_authenticated and request.user.is_staff:
        if (request.method == "POST"):
            cliente = nuevoClienteForm(request.POST)
            if (cliente.is_valid()):
                nuevo_cliente = cliente.save()
                nuevo_cliente.save()

                messages.success(request, 'Se ha agregado correctamente el Cliente {}'.format(
                    nuevo_cliente))
                return redirect('persona:registrar_cliente')
        else:
            cliente = nuevoClienteForm()

        return render(request, 'persona/cliente.html', {'form': cliente})



@permission_required('persona.change_Cliente', login_url="usuario:login")
def modificar_cliente(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        cliente = get_object_or_404(Cliente, id=id)
        data = {
            'form': nuevoClienteForm(instance=cliente)
        }
        if (request.method == "POST"):

            formulario = nuevoClienteForm(
                data=request.POST, instance=cliente, files=request.FILES)

            if formulario.is_valid():
                formulario.save()
                data["mensaje"] = "Modificacion correcta"

                return redirect(to="persona:listar_clientes")

            data["form"] = formulario

        return render(request, 'persona/modificar_cliente.html', data)

@permission_required('persona.delete_Cliente', login_url="usuario:login")
def eliminar_cliente(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        cliente = get_object_or_404(Cliente, id=id)
        cliente.delete()
        return redirect(to='persona:listar_clientes')


############################## EMPLEADO##############################

@permission_required('persona.view_Empleado', login_url="usuario:login")
def listar_empleados(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'persona/listar_empleados.html', {'empleados': Empleado.objects.all()})


@permission_required('persona.add_Empleado', login_url="usuario:login")
def creacion_empleado(request):
    if request.user.is_authenticated and request.user.is_staff:
        if (request.method == "POST"):
            empleado = nuevoEmpleadoForm(request.POST)

            if (empleado.is_valid()):
                nuevo_empleado = empleado.save()

                nuevo_empleado.save()

                messages.success(
                    request, 'se ha creado el Empleado exitosamente')

                return redirect(to="persona:registrar_empleado")

        else:
            empleado = nuevoEmpleadoForm()

        return render(request, 'persona/empleado.html', {'empleado': empleado})


@permission_required('persona.delete_Empleado', login_url="usuario:login")
def eliminar_empleado(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        empleado = get_object_or_404(Empleado, id=id)
        empleado.delete()
        return redirect(to='persona:listar_empleados')


@permission_required('persona.change_Empleado', login_url="usuario:login")
def modificar_empleado(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        empleado = get_object_or_404(Empleado, id=id)
        data = {
            'empleado': nuevoEmpleadoForm(instance=empleado)
        }
        if (request.method == "POST"):

            formulario = nuevoEmpleadoForm(

                data=request.POST, instance=empleado, files=request.FILES)

            if formulario.is_valid():
                formulario.save()
                data["mensaje"] = "Modificacion correcta"

                return redirect(to="persona:listar_empleados")

            data["empleado"] = formulario

        return render(request, 'persona/modificar_empleado.html', data)


############################## CADETE##############################

@permission_required('persona.add_Repartidor', login_url="usuario:login")
def creacion_repartidor(request):
    if request.user.is_authenticated and request.user.is_staff:
        if (request.method == "POST"):
            repartidor = nuevoRepartidorForm(request.POST)
            if (repartidor.is_valid()):
                nuevo_repartidor = repartidor.save()
                nuevo_repartidor.save()

                messages.success(request, 'Se ha agregado correctamente el Repartidor {}'.format(
                    nuevo_repartidor))
                return redirect('persona:creacion_repartidor')
        else:
            repartidor = nuevoRepartidorForm()

        return render(request, 'persona/repartidor.html', {'repartidor': repartidor})

@permission_required('persona.delete_Repartidor', login_url="usuario:login")
def eliminar_repartidor(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        repartidor = get_object_or_404(Repartidor, id=id)
        repartidor.delete()
        return redirect(to='persona:listar_repartidores')

@permission_required('persona.change_Repartidor', login_url="usuario:login")
def modificar_repartidor(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        repartidor = get_object_or_404(Repartidor, id=id)
        data = {
            'repartidor': nuevoRepartidorForm(instance=repartidor)
        }
        if (request.method == "POST"):

            formulario = nuevoRepartidorForm(

                data=request.POST, instance=repartidor, files=request.FILES)

            if formulario.is_valid():
                formulario.save()
                data["mensaje"] = "Modificacion correcta"

                return redirect(to="persona:listar_repartidores")

            data["repartidor"] = formulario

        return render(request, 'persona/modificar_repartidor.html', data)

@permission_required('persona.viwe_Repartidor', login_url="usuario:login")
def listar_repartidores(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'persona/listar_repartidores.html', {'repartidores': Repartidor.objects.all()})

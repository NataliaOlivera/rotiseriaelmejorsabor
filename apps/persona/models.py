from datetime import datetime
from django.db import models
from enum import unique

from django.forms import ValidationError


# Create your models here.


class Persona(models.Model):
    CUIT = models.CharField(max_length=11, unique=True)
    apellido = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    celular = models.DecimalField(
        blank=True, null=True, max_digits=78, decimal_places=0)
    telefono_fijo = models.DecimalField(
        blank=True, null=True, max_digits=78, decimal_places=0)
    fecha_nacimiento = models.DateField()
    calle = models.CharField(max_length=30)
    numero = models.IntegerField()
    localidad = models.CharField(max_length=30)
    departamento = models.CharField(max_length=30)
    observaciones = models.TextField(max_length=1000, blank=True)
    estado = models.BooleanField(default=True)

    class Meta:
        abstract = True

    def validar_fecha_nacimiento(self):
        fecha_actual = datetime.now.strftime("%Y-%m-%d")
        if (self.fecha_nacimiento.strftime("%Y-%m-%d") > fecha_actual):
            raise ValidationError(
                'La fecha de nacimiento es mayor a la actual')


class Cliente(Persona):

    def __str__(self):
        return "%s %s %s %s" % (self.CUIT, self.apellido, self.nombre, self.calle)


class Empleado(Persona):
    EMPLEADO = 'E'

    fecha_ingreso_trabajar = models.DateField()

    def __str__(self):
        return "%s %s %s %s" % (self.CUIT, self.apellido, self.nombre)


class Repartidor(Persona):
    NORTE = 'N'
    SUR = 'S'
    ESTE = 'E'
    OESTE = 'O'

    ZONA = [
        (NORTE, 'NORTE'),
        (SUR, 'SUR'),
        (ESTE, 'ESTE'),
        (OESTE, 'OESTE')
    ]
    fecha_ingreso_trabajar = models.DateField()
    vigencia_carnet = models.DateField()
    patente = models.CharField(max_length=30)
    zona = models.CharField(max_length=1, choices=ZONA)

    def __str__(self):
        return " %s %s  %s" % ( self.apellido, self.nombre, self.patente)

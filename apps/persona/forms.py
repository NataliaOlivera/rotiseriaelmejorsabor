from django import forms
from django.forms import DateInput
from .models import Cliente, Empleado, Repartidor


class nuevoClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente

        fields = (
            "CUIT", "apellido", "nombre", "celular", "telefono_fijo", "fecha_nacimiento", "calle", "numero",
            "localidad",
            "departamento", "observaciones")

        widgets = {

            'fecha_nacimiento': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

        }


class nuevoEmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado

        fields = (
            "CUIT", "apellido", "nombre", "celular", "telefono_fijo", "fecha_nacimiento", "calle", "numero",
            "localidad", "departamento", "observaciones", "fecha_ingreso_trabajar"
        )

        widgets = {

            'fecha_nacimiento': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

            'fecha_ingreso_trabajar': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

        }


class nuevoRepartidorForm(forms.ModelForm):
    class Meta:
        model = Repartidor

        fields = (
            "CUIT", "apellido", "nombre", "celular", "telefono_fijo", "fecha_nacimiento", "calle", "numero",
            "localidad", "departamento", "observaciones", "fecha_ingreso_trabajar", "vigencia_carnet",
            "patente", "zona"
        )

        widgets = {

            'fecha_nacimiento': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

            'fecha_ingreso_trabajar': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

            'vigencia_carnet': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),

        }

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from apps.persona.forms import nuevoClienteForm
from .forms import UsuarioForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.urls import reverse


@login_required(login_url='usuario:login')
def index(request):
    return render(request, "/")


def login_view(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect("pedido:Menu_pagina_principal")
        else:
            return render(request, "usuario/login.html", {"msj": "Credenciales incorrectas"})
    return render(request, "usuario/login.html")


def logout_view(request):
    logout(request)
    return render(request, "usuario/login.html", {"msj": "Deslogueado"})


def registro(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse("usuario:login"))
    if request.method == 'POST':
        usuario_form = UsuarioForm(request.POST, request.FILES)
        cliente_form = nuevoClienteForm(request.POST, request.FILES)

        if usuario_form.is_valid() and cliente_form.is_valid():
            nuevo_cliente = cliente_form.save()
            usuario_form.instance.Cliente = nuevo_cliente
            nuevo_usuario = usuario_form.save()
            user = authenticate(
                username=usuario_form.cleaned_data["username"], password=usuario_form.cleaned_data["password1"])
            login(request, user)
            messages.success(request, "Usuario Registrado Exitosamente")
            return redirect(to="pedido:Menu_pagina_principal")
    else:
        usuario_form = UsuarioForm()
        cliente_form = nuevoClienteForm()
    context = {
        'form': usuario_form,
        'formC': cliente_form
    }
    return render(request, "usuario/registro.html", context)

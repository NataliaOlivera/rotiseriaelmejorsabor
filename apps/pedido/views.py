import datetime
from operator import itemgetter
from django.http import Http404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from .forms import DateRangeForm, DateRangeCadeteForm
from apps.pedido.carro import Carro
from apps.persona.models import Repartidor
from .forms import ActualizarPedidoForm, PedidoClienteForm, PedidoForm, nuevoMenuForm, nuevoTipoMenuForm, nuevoTipoPlatoForm
from .models import Pedido, Estado, Menu, Tipo_Plato, Tipo_Menu
from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.urls import reverse
from django.db.models import Q
from django.core.paginator import Paginator


def compra(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/compra.html')


def estadisticas(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/estadisticas.html')


def estado_pedido(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/estado_pedido.html')


def orden_compra(request):
    return render(request, 'pedido/orden_compra.html')


def pedidos_vista_admin(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/pedidos_vista_admin.html')


def menu(request):
    return render(request, 'pedido/menu.html')


####################################################################################


def listar_menu(request):
    return render(request, 'pedido/listar_menu.html', {'menus': Menu.objects.all()})


def gestion_menu(request):
    if request.user.is_authenticated and request.user.is_staff:
        if (request.method == "POST"):
            menu = nuevoMenuForm(request.POST)

            for field in menu:
                print("Field Error:", field.name, field.errors)

            if (menu.is_valid()):
                nuevo_menu = menu.save()
                nuevo_menu.save()

                messages.success(request, 'Se ha agregado correctamente el menu {}'.format(
                    nuevo_menu))
                return redirect('pedido:gestion_menu')

        else:
            menu = nuevoMenuForm()

        return render(request, 'pedido/gestion_menu.html', {'menu': menu})


def eliminar_menu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        menu = get_object_or_404(Menu, id=id)
        menu.delete()
        return redirect(to='pedido:listar_menu')


def modificar_menu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        menu = get_object_or_404(Menu, id=id)
        data = {
            'menu': nuevoMenuForm(instance=menu)
        }
        if (request.method == "POST"):

            formulario = nuevoMenuForm(

                data=request.POST, instance=menu, files=request.FILES)

            if formulario.is_valid():
                formulario.save()
                data["mensaje"] = "Modificacion correcta"

                return redirect(to="pedido:listar_menu")

            data["menu"] = formulario

        return render(request, 'pedido/modificar_menu.html', data)


##########################################################################################


def gestion_plato(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/gestion_plato.html')


def gestion_ingrediente(request):
    if request.user.is_authenticated and request.user.is_staff:
        return render(request, 'pedido/gestion_Ingrediente.html')


############################################################################################




def Menu_pagina_principal(request):
    busqueda = request.POST.get("buscador")
    product_list = Menu.objects.order_by('nombre')
    page = request.GET.get('page', 1)

    if busqueda:
        product_list = Menu.objects.filter(
            Q(nombre__icontains=busqueda) |
            Q(descripcion__icontains=busqueda)
        ).distinct()

    try:
        paginator = Paginator(product_list, 12)
        product_list = paginator.page(page)
    except:
        raise Http404

    data = {'entity': product_list,
            'paginator': paginator
            }
    return render(request, 'pedido/menuDeComidas.html', data)



def menuxCategoria(request, id):
    busqueda = request.POST.get("buscador")
    lista_productos = Menu.objects.filter(tipo_menu=id)

    if busqueda:
        lista_productos = Menu.objects.filter(
            Q(nombre__icontains=busqueda) |
            Q(descripcion__icontains=busqueda)
        ).distinct()

    data = {'entity': lista_productos}
    return render(request, 'pedido/menuDeComidas.html', data)


def detalleMenu(request, id):
    menu = get_object_or_404(Menu, id=id)
    otrosProductos = Menu.objects.filter(tipo_menu=menu.tipo_menu)
    data = {
        'menu': menu,
        'productosRelacionados': otrosProductos
    }
    return render(request, 'pedido/menu/detalle.html', data)


@login_required(login_url='/login')
def addMenu(request):
    data = {
        'form': nuevoMenuForm()
    }

    if request.method == 'POST':
        formulario = nuevoMenuForm(data=request.POST, files=request.FILES)

        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Registro agregado correctamente")
            return redirect(to="pedido:listarMenues")
        else:
            data["form"] = formulario
    return render(request, 'pedido/menu/agregar.html', data)


def listarMenues(request):
    if request.user.is_authenticated and request.user.is_staff:
        busqueda = request.POST.get("buscador")
        lista_productos = Menu.objects.order_by('nombre')
        page = request.GET.get('page', 1)
        if busqueda:
            lista_productos = Menu.objects.filter(
                Q(nombre__icontains=busqueda) |
                Q(descripcion__icontains=busqueda)
            ).distinct()

        try:
            paginator = Paginator(lista_productos, 6)
            lista_productos = paginator.page(page)
        except:
            raise Http404

        data = {'entity': lista_productos,
                'title': 'LISTADO DE MENUES',
                'paginator': paginator
                }
        return render(request, 'pedido/menu/listar.html', data)


def editarMenu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        menu = get_object_or_404(Menu, id=id)
        data = {
            'form': nuevoMenuForm(instance=menu)
        }

        if request.method == 'POST':
            formulario = nuevoMenuForm(
                data=request.POST, instance=menu, files=request.FILES)
            if formulario.is_valid():
                formulario.save()
                messages.success(request, "Registro modificado correctamente")
                return redirect(to="pedido:listarMenues")
            data["form"] = formulario
        return render(request, 'pedido/menu/modificar.html', data)


def deleteMenu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        producto = get_object_or_404(Menu, id=id)
        producto.delete()
        messages.success(request, "Registro eliminado correctamente")
        return redirect(to="pedido:listarMenues")

@permission_required('pedido.view_Tipo_Menu', login_url="usuario:login")
def listTipo_Menu(request):
    lista_categorias = Tipo_Menu.objects.all().order_by('nombre')
    page = request.GET.get('page', 1)

    try:
        paginator = Paginator(lista_categorias, 6)
        lista_categorias = paginator.page(page)
    except:
        raise Http404

    data = {'entity': lista_categorias,
            'title': 'Tipo De Menu',
            'paginator': paginator
            }

    return render(request, 'pedido/tipo_menu.html', data)

@permission_required('pedido.add_Tipo_Menu', login_url="usuario:login")
def addTipo_Menu(request):
    if request.user.is_authenticated and request.user.is_staff:
        data = {
            'form': nuevoTipoMenuForm()
        }
        if request.method == 'POST':
            formulario = nuevoTipoMenuForm(data=request.POST)

            if formulario.is_valid():
                formulario.save()
                messages.success(request, "Registro agregado correctamente")
                return redirect(to="pedido:tipo_menu")
            else:
                data["form"] = formulario
        return render(request, 'pedido/tipo_menu/agregar_tipo_menu.html', data)

@permission_required('pedido.change_Tipo_Menu', login_url="usuario:login")
def modificarTipo_Menu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        tipo_menu = get_object_or_404(Tipo_Menu, id=id)

        data = {
            'form': nuevoTipoMenuForm(instance=tipo_menu)
        }
        if request.method == 'POST':
            formulario = nuevoTipoMenuForm(
                data=request.POST, instance=tipo_menu)
            if formulario.is_valid():
                formulario.save()
                messages.success(request, "Registro modificado correctamente")
                return redirect(to="pedido:tipo_menu")
            else:
                data["form"] = formulario

        return render(request, 'pedido/tipo_menu/modificar_tipo_menu.html', data)

@permission_required('pedido.delete_Tipo_Menu', login_url="usuario:login")
def deleteTipo_Menu(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        categoria = get_object_or_404(Tipo_Menu, id=id)
        categoria.delete()
        messages.success(request, "Registro eliminado correctamente")
        return redirect(to="pedido:tipo_menu")


def listTipo_Plato(request):
    if request.user.is_authenticated and request.user.is_staff:
        lista_categorias = Tipo_Plato.objects.all().order_by('nombre')
        page = request.GET.get('page', 1)

        try:
            paginator = Paginator(lista_categorias, 6)
            lista_categorias = paginator.page(page)
        except:
            raise Http404

        data = {'entity': lista_categorias,
                'title': 'LISTADO DE  TIPOS DE PLATOS',
                'paginator': paginator
                }

        return render(request, 'pedido/tipo_plato.html', data)


def addTipo_Plato(request):
    if request.user.is_authenticated and request.user.is_staff:
        data = {
            'form': nuevoTipoPlatoForm()
        }
        if request.method == 'POST':
            formulario = nuevoTipoPlatoForm(data=request.POST)

            if formulario.is_valid():
                formulario.save()
                messages.success(request, "Registro agregado correctamente")
                return redirect(to="pedido:tipo_plato")
            else:
                data["form"] = formulario
        return render(request, 'pedido/tipo_plato/agregar_tipo_plato.html', data)


def modificarTipo_Plato(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        tipo_plato = get_object_or_404(Tipo_Plato, id=id)

        data = {
            'form': nuevoTipoPlatoForm(instance=tipo_plato)
        }
        if request.method == 'POST':
            formulario = nuevoTipoPlatoForm(
                data=request.POST, instance=tipo_plato)
            if formulario.is_valid():
                formulario.save()
                messages.success(request, "Registro modificado correctamente")
                return redirect(to="pedido:tipo_plato")
            else:
                data["form"] = formulario

        return render(request, 'pedido/tipo_plato/modificar_tipo_plato.html', data)


def deleteTipo_plato(request, id):
    if request.user.is_authenticated and request.user.is_staff:
        tipo_tlato = get_object_or_404(Tipo_Plato, id=id)
        tipo_tlato.delete()
        messages.success(request, "Registro eliminado correctamente")
        return redirect(to="pedido:tipo_plato")

#################CARRO###########################
@login_required(login_url='/login')
def viewcart(request):
    return render(request, 'pedido/carrito/cart.html', {'carro': request.session['carro']})


@login_required(login_url='/login')
def agregar_Menu(request, producto_id):
    carro = Carro(request)
    producto = Menu.objects.get(id=producto_id)
    carro.agregar(producto=producto)
    return redirect(to="pedido:Menu_pagina_principal")


@login_required(login_url='/login')
def agregar_Menu_procesar_compra(request, producto_id):
    carro = Carro(request)
    producto = Menu.objects.get(id=producto_id)
    carro.agregar(producto=producto)
    return redirect(to="pedido:viewcart")


@login_required(login_url='/login')
def agregar_Menu_detalle(request, producto_id):
    carro = Carro(request)
    producto = Menu.objects.get(id=producto_id)
    carro.agregar(producto=producto)
    return redirect(to="pedido:detalle")


@login_required(login_url='/login')
def eliminar_Menu(request, producto_id):
    carro = Carro(request)
    producto = Menu.objects.get(id=producto_id)
    carro.eliminar(producto=producto)
    return redirect(to="pedido:viewcart")


@login_required(login_url='/login')
def restar_Menu(request, producto_id):
    carro = Carro(request)
    producto = Menu.objects.get(id=producto_id)
    carro.restar(producto=producto)
    return redirect(to="pedido:viewcart")


@login_required(login_url='/login')
def cleancart(request):
    carro = Carro(request)
    carro.limpiar_carro()
    return redirect(to="pedido:viewcart")


@login_required(login_url='/login')
def cleancart2(request):
    carro = Carro(request)
    carro.limpiar_carro()
    return redirect(to="pedido:Menu_pagina_principal")


@login_required(login_url='/login')
def procesar_compra(request):
    messages.success(request, 'Gracias por su Compra!!')
    carro = Carro(request)
    sess = request.session.get("data", {"items": []})
    carro.limpiar_carro()
    return redirect('pedido:Menu_pagina_principal')


#######################PEDIDOS###########################

def registroPedido(request):
    context = {}
    if request.user.is_authenticated and request.user.is_staff:
        nuevo_pedido = None
        if request.method == 'POST':
            pedidoAdminform = PedidoForm(request.POST, request.FILES)
            if pedidoAdminform.is_valid():
                nuevo_pedido = pedidoAdminform.save(commit=True)
                messages.success(request, 'Pedido Exitoso')
                return redirect(reverse('Pedido:registroPedido'))
        else:
            pedidoAdminform = PedidoForm()
        context = {
            'form': pedidoAdminform
        }
        return render(request, 'Pedido/realizarPedido.html', context)
    else:
        return render(request, 'pedido/Menu_pagina_principal', context)


@login_required(login_url='/login')
def PedidoCliente(request):

    cliente = request.user.Cliente
    domicilio_cliente = cliente.localidad
    carro = Carro(request)


    fechaActual = datetime.date.today()
    horaActual = datetime.datetime.now()
    horaEntrega = horaActual + datetime.timedelta(minutes=30)

    if request.method == 'POST':
        pedidoClienteform = PedidoClienteForm(request.POST, request.FILES)
      
        nuevo_pedido = pedidoClienteform.save(commit=False)
        nuevo_pedido.fechaPedido = fechaActual
        nuevo_pedido.horaEntrega = horaEntrega

        nuevo_pedido.cliente = cliente
        
        estadoPendiente = Estado.objects.get(pk=1)
        nuevo_pedido.Estado = estadoPendiente

        print(carro.carro)
        nuevo_pedido.save()
        for key in carro.carro:
            menuRegistrado = Menu.objects.get(id=key)
            print(menuRegistrado)
            nuevo_pedido.menu.add(menuRegistrado)

        if pedidoClienteform.is_valid():
            messages.success(
                request, 'Se registro con exito el Pedido'.format(nuevo_pedido))
            cleancart2(request)
            return redirect(reverse('pedido:Menu_pagina_principal'))
    else:
        pedidoClienteform = PedidoClienteForm()

    context = {
        'form': pedidoClienteform,
        'cliente': cliente,
        'repartidor': Repartidor,
        'horaEntrega': horaEntrega.time(),
        'fechaActual': fechaActual,
    }

    return render(request, 'Pedido/pedidoCliente.html', context)

#######################################################


def PedidoAdmin(request):

    context = {}
    carro = Carro(request)
    if request.user.is_authenticated and request.user.is_staff:

        if request.method == 'POST':
            pedidoAdminform = PedidoForm(request.POST, request.FILES)
            nuevo_pedido = pedidoAdminform.save(commit=False)

            print(carro.carro)
            nuevo_pedido.save()
        

        
            for key in carro.carro:
                menuRegistrado = Menu.objects.get(id=key)
                print(menuRegistrado)
                nuevo_pedido.menu.add(menuRegistrado)

                
            if pedidoAdminform.is_valid():
                messages.success(
                    request, 'Se ha registrado correctamente el pedido')
                cleancart2(request)
                return redirect(reverse('pedido:Menu_pagina_principal'))
        
        else:
            pedidoAdminform = PedidoForm()
        context = {
            'form': pedidoAdminform
        }
        return render(request, 'pedido/pedidoAdmin.html', context)
    else:
        return render(request, 'pedido/menuDeComidas.html', context)

    ####################LISTAR PERDIDOS###########





@login_required(login_url='/login')
def listar_pedidos_clientes(request):
    if request.user.is_authenticated:
       

        cliente = request.user.Cliente

        pedidosCliente = Pedido.objects.filter(cliente=cliente)
        formEstado = ActualizarPedidoForm()
     

        context = {
            'pedidos': pedidosCliente,
            'form': formEstado,
           
        }
        return render(request, 'pedido/estadoPedidosClientes.html', context)
    else:
        return redirect(reverse('pedido:Menu_pagina_principal'))




@login_required(login_url='/login')
def todos_los_pedido_lista(request):
    if request.user.is_authenticated and request.user.is_staff:
        pedidos = Pedido.objects.all()
        repartidores = Repartidor.objects.all()
        formEstado = ActualizarPedidoForm()
        context = {
            'pedidos': pedidos,
            'form': formEstado,
            'repartidores':repartidores
        }
        return render(request, 'pedido/estadoPedidosClientes.html', context)
    else:
        return redirect(reverse('pedido:Menu_pagina_principal'))


def cambiarEstadoPedidoyComentar(request, pedido_id):
    if request.user.is_authenticated and request.user.is_staff:
        if request.method == 'POST':
            formEstado = ActualizarPedidoForm(request.POST, request.FILES)
            if formEstado.is_valid():
                estado_id = formEstado.cleaned_data['estadoPedido']
                comentario = formEstado.cleaned_data['comentario']

                pedido = Pedido.objects.filter(pk=pedido_id).first()
                estadoPedido = Estado.objects.filter(pk=estado_id).first()
                pedido.Estado = estadoPedido
                pedido.comentario = comentario

                pedido.save()
        if request.user.groups.filter(name='Repartidor').exists():
            return redirect(reverse('pedido:todos_los_pedido_lista'))
        else:
            return redirect(reverse('pedido:todos_los_pedido_lista'))

# def asignarRepartidor(request, id):
#     if request.user.is_authenticated and request.user.is_staff:
#         pedido = Pedido.objects.filter(pk=id).first()
   
#         context = {
#             'pedido': pedido,
#         }
#         return render(request, 'pedido/designar_cadete.html', context)
      








# def asignarRepartidor(request, id):
#     if request.user.is_authenticated and request.user.is_staff:
#         #pedido = get_object_or_404(Pedido, id=id)
#         pedido = Pedido.objects.filter(pk=id)
#         data = {
#             'form': PedidoForm(instance=Pedido)
#         }
#         if request.method == 'POST':
#             formulario = PedidoForm(
#                 data=request.POST)
#             if formulario.is_valid():
#                 formulario.save()
#                 messages.success(request, "Cadete asignado correctamente")
#                 return redirect(to="pedido:todos_los_pedido_lista")
#             else:
#                 data["form"] = formulario

#         return render(request, 'pedido/designar_cadete.html', data)







def asignarRepartidor(request, id):
    pedido = get_object_or_404(Pedido, id=id)

    data = {
        'form': PedidoForm(instance=pedido),
        'id': id

    }
    if request.method == 'POST':
        formulario = PedidoForm(data=request.POST, instance=pedido)
        print("antes if")
        if formulario.is_valid():
            formulario.save()
            print("se guardo")
            messages.success(request, "Cambios Registrados")
            return redirect(reverse("pedido:todos_los_pedido_lista"))
        data["form"] = formulario
    return render(request, 'pedido/designar_cadete.html', data )








################ESTADISTICAS###############
def estadisticas(request):  
    rangoFechasPedidosDiarios = DateRangeForm()
    rangoFechasCantidadesCadete = DateRangeCadeteForm()
    rangoFechasPlatos = DateRangeForm()
    rangoFechasMenus = DateRangeForm()

    context = {  
        'formPedidos': rangoFechasPedidosDiarios,
        'formMenus': rangoFechasMenus,
        'formCadete': rangoFechasCantidadesCadete,
        'formPlatos': rangoFechasPlatos,
    }
    return render(request, 'pedido/estadisticas.html', context)



def listar_pedidos_por_fecha(request):
    pedidos = None  
    if request.method == 'POST':  
        rangoFechasPedidosDiarios = DateRangeForm(request.POST, request.FILES)
        if rangoFechasPedidosDiarios.is_valid():
            fecha_desde = rangoFechasPedidosDiarios[
                'Fecha_desde'].value()  
            fecha_hasta = rangoFechasPedidosDiarios['Fecha_hasta'].value()
            pedidos = Pedido.objects.filter(fechaPedido__range=[fecha_desde,
                                                                fecha_hasta])
    else:
        rangoFechasPedidosDiarios = DateRangeForm()

    rangoFechasPedidosDiarios = DateRangeForm()
    rangoFechasCantidadesCadete = DateRangeCadeteForm()
    rangoFechasPlatos = DateRangeForm()
    rangoFechasMenus = DateRangeForm()

    context = {
        'formPedidos': rangoFechasPedidosDiarios,
        'formMenus': rangoFechasMenus,
        'formCadete': rangoFechasCantidadesCadete,
        'formMenu': rangoFechasPlatos,
        'pedidos': pedidos,
    }
    return render(request, 'pedido/estadisticas.html', context)


def listar_pedidos_por_fecha(request):  
    pedidos = None  
    if request.method == 'POST': 
        rangoFechasPedidosDiarios = DateRangeForm(request.POST, request.FILES)
        if rangoFechasPedidosDiarios.is_valid():
            fecha_desde = rangoFechasPedidosDiarios[
                'Fecha_desde'].value()  
            fecha_hasta = rangoFechasPedidosDiarios['Fecha_hasta'].value()
            pedidos = Pedido.objects.filter(fechaPedido__range=[fecha_desde,
                                                                fecha_hasta]) 
    else:
        rangoFechasPedidosDiarios = DateRangeForm() 

    rangoFechasPedidosDiarios = DateRangeForm()
    rangoFechasCantidadesCadete = DateRangeCadeteForm()
    rangoFechasPlatos = DateRangeForm()
    rangoFechasMenus = DateRangeForm()

    context = {
        'formPedidos': rangoFechasPedidosDiarios,
        'formMenus': rangoFechasMenus,
        'formCadete': rangoFechasCantidadesCadete,
        'formMenu': rangoFechasPlatos,
        'pedidos': pedidos,
    }
    return render(request, 'pedido/estadisticas.html', context)







def ListarMenuesSolicitados(request):
    menuesSolicitados = []
    if request.method == 'POST':
        rangoFechasMenus = DateRangeForm(request.POST, request.FILES)
        if rangoFechasMenus.is_valid():
            fecha_desde = rangoFechasMenus['Fecha_desde'].value()
            fecha_hasta = rangoFechasMenus['Fecha_hasta'].value()

            platos = Menu.objects.all()
            for plato in platos:  
                cantidadSolicitada = cantidadSolicitadaMenu(plato, fecha_desde, fecha_hasta)
                tuplaPlato = (plato.id, plato.nombre, plato.tipo_plato.nombre, plato.precio, plato.tipo_menu.nombre,cantidadSolicitada)
                menuesSolicitados.append(tuplaPlato)
            menuesSolicitados = sorted(menuesSolicitados, key=itemgetter(5),
                                       reverse=True)  
            menuesSolicitados = menuesSolicitados[
                                :5]  
    else:
        rangoFechasMenus = DateRangeForm()
    print(rangoFechasMenus.errors.as_data())

    rangoFechasPedidosDiarios = DateRangeForm()
    rangoFechasCantidadesCadete = DateRangeCadeteForm()
    rangoFechasPlatos = DateRangeForm()
    context = {
        'formPedidos': rangoFechasPedidosDiarios,
        'formMenus': rangoFechasMenus,
        'formCadete': rangoFechasCantidadesCadete,
        'formPlatos': rangoFechasPlatos,
        'menuesSolicitados': menuesSolicitados,
    }
    return render(request, 'pedido/estadisticas.html', context)




def cantidadSolicitadaMenu(menu, fecha_desde,
                            fecha_hasta): 
    pedidos = Pedido.objects.filter(fechaPedido__range=[fecha_desde, fecha_hasta])
    contador = 0
    for pedido in pedidos:
        menuesDelPedido = pedido.menu.all()
        for platoDelPedido in menuesDelPedido:
            if menu.id == platoDelPedido.id:
                contador += 1
    return contador



from django.contrib import admin

from .models import *

admin.site.register(Tipo_Plato)
admin.site.register(Tipo_Menu)
admin.site.register(Menu)
admin.site.register(Pedido)
admin.site.register(Estado)
admin.site.register(TipoEntrega)

# Generated by Django 4.1.3 on 2022-11-10 01:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('persona', '0004_alter_cliente_celular_alter_cliente_telefono_fijo_and_more'),
        ('pedido', '0008_estado_pedido_tipoentrega_remove_vianda_cadete_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='Estado',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pedido.estado'),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='cliente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='persona.cliente'),
        ),
        migrations.AlterField(
            model_name='pedido',
            name='horaEntrega',
            field=models.TimeField(blank=True),
        ),
    ]

from django.urls import path

from .views import ListarMenuesSolicitados, Pedido, PedidoAdmin, PedidoCliente, addTipo_Plato, agregar_Menu_detalle, agregar_Menu_procesar_compra, cambiarEstadoPedidoyComentar, deleteTipo_plato, listTipo_Menu, listTipo_Plato, listar_pedidos_clientes, listar_pedidos_por_fecha, \
    modificarTipo_Plato, Menu_pagina_principal, \
     \
    detalleMenu, addMenu, menuxCategoria, editarMenu, deleteMenu, listarMenues, addTipo_Menu, \
    modificarTipo_Menu, deleteTipo_Menu, registroPedido, todos_los_pedido_lista, viewcart, agregar_Menu, eliminar_Menu, restar_Menu, cleancart, procesar_compra, estadisticas, asignarRepartidor

app_name = 'pedido'

urlpatterns = [

    path('menu/', Menu_pagina_principal, name='Menu_pagina_principal'),
    path('agregar_Menu_procesar_compra/<producto_id>',
         agregar_Menu_procesar_compra, name="agregar_Menu_procesar_compra"),

    path('tipo_menu/', listTipo_Menu, name='tipo_menu'),

    path('tipo_plato/', listTipo_Plato, name='tipo_plato'),

    path('addTipo_Plato/', addTipo_Plato, name='addTipo_Plato'),

    path('modificarTipo_Plato/<id>', modificarTipo_Plato,
         name='modificarTipo_Plato'),

    path('deleteTipo_plato/<id>', deleteTipo_plato, name='deleteTipo_plato'),

    path('addTipo_Menu/', addTipo_Menu, name='addTipo_Menu'),

    path('modificarTipo_Menu/<id>', modificarTipo_Menu, name='modificarTipo_Menu'),

    path('deleteTipo_Menu/<id>', deleteTipo_Menu, name='deleteTipo_Menu'),

    path('detalle/', detalleMenu, name='detalle'),

    path('addMenu/', addMenu, name='addMenu'),

    path('menuxCategoria/<id>/', menuxCategoria, name='menuxCategoria'),

    path('editarMenu/<id>/', editarMenu, name='editarMenu'),

    path('deleteMenu/<id>/', deleteMenu, name='deleteMenu'),

    path('listarMenues/', listarMenues, name='listarMenues'),


    path('viewcart/', viewcart, name="viewcart"),

    path('agregar_Menu/<producto_id>', agregar_Menu, name="agregar_Menu"),


    path('delcart/<producto_id>', eliminar_Menu, name="delcart"),

    path('restarcart/<producto_id>', restar_Menu, name="restarcart"),

    path('cleancart/', cleancart, name="cleancart"),

    path('procesar_compra/', procesar_compra, name="procesar_compra"),

    path('registroPedido/', registroPedido, name="registroPedido"),
    path('pedidoCliente/', PedidoCliente, name="pedidoCliente"),



    path('PedidoAdmin/', PedidoAdmin, name="PedidoAdmin"),


    path('listar_pedidos_clientes/', listar_pedidos_clientes,
         name="listar_pedidos_clientes"),

    path('todos_los_pedido_lista/', todos_los_pedido_lista,
         name="todos_los_pedido_lista"),





    path('cambiarEstadoPedidoyComentar/<int:pedido_id>',
         cambiarEstadoPedidoyComentar, name="cambiarEstadoPedidoyComentar"),

    path('estadisticas/', estadisticas, name="estadisticas"),

    path('listar_pedidos_por_fecha', listar_pedidos_por_fecha,
         name="listar_pedidos_por_fecha"),

    path('ListarMenuesSolicitados', ListarMenuesSolicitados,
         name="ListarMenuesSolicitados"),



    path('asignarRepartidor/<id>', asignarRepartidor, name='asignarRepartidor'),




]

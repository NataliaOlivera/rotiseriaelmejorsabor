from datetime import date
from distutils.command.upload import upload
from django.db import models
from model_utils import Choices
from apps.persona.models import Cliente
from django.contrib.auth.models import User


class Tipo_Plato(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = "Tipo De Plato"
        verbose_name = 'Tipo De Plato'
        verbose_name_plural = 'Tipos De Platos'
        ordering = ['id']


class Tipo_Menu(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = "Tipo De Menu"
        verbose_name = 'Tipo De Menu'
        verbose_name_plural = 'Tipos De Menues'
        ordering = ['id']


class Menu(models.Model):
    nombre = models.CharField(max_length=30)
    imagen = models.ImageField(upload_to='images/', blank=True)
    descripcion = models.CharField(max_length=500, blank=True)
    precio = models.IntegerField()
    tipo_menu = models.ForeignKey(Tipo_Menu, on_delete=models.PROTECT)
    tipo_plato = models.ForeignKey(Tipo_Plato, on_delete=models.PROTECT)

    vigente = models.BooleanField(default=True)

    def __str__(self):
        return "%s %s %s %s " % (self.nombre, self.precio, self.tipo_menu, self.tipo_plato)


class Pedido(models.Model):

    cliente = models.ForeignKey(
        "persona.Cliente", on_delete=models.CASCADE, null=False)

    fechaPedido = models.DateField()
    horaEntrega = models.TimeField(blank=True)
    TipoEntrega = models.ForeignKey(
        "TipoEntrega", on_delete=models.CASCADE, null=True)
    Estado = models.ForeignKey(
        "Estado", on_delete=models.CASCADE, null=True, blank=True)
    menu = models.ManyToManyField(Menu, blank=True)
    repartidor = models.ForeignKey(
        "persona.Repartidor", on_delete=models.CASCADE, null=True, blank=True)

    comentario = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.cliente.nombre + ' ' + self.cliente.apellido + '-' + self.Estado.estadoPedido + '-' + self.TipoEntrega.tipoEntrega


class Estado(models.Model):
    ESTADO_PEDIDO = Choices("Pendiente", "En preparación",
                            "En camino", "Entregado", "Devuelto", "Cancelado")
    PENDIENTE = "Pendiente"
    estadoPedido = models.CharField(
        max_length=15, choices=ESTADO_PEDIDO, default=PENDIENTE, unique=True)

    def __str__(self):
        return self.estadoPedido


class TipoEntrega(models.Model):
    TIPO_ENTREGA = Choices("Delivery", "Retiro")
    tipoEntrega = models.CharField(
        max_length=8, choices=TIPO_ENTREGA, default="Retiro", unique=True)

    def __str__(self):
        return self.tipoEntrega

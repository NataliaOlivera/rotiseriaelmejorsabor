from django import forms
from django.forms import DateInput, ModelForm
from .models import Menu, Pedido, Tipo_Menu, Tipo_Plato

from apps.persona.models import *


class nuevoMenuForm(forms.ModelForm):
    class Meta:
        model = Menu

        fields = (
            "nombre", "precio", "descripcion", "tipo_menu", "tipo_plato", "imagen")


class nuevoTipoMenuForm(forms.ModelForm):
    class Meta:
        model = Tipo_Menu
        fields = ["nombre"]


class nuevoTipoPlatoForm(forms.ModelForm):
    class Meta:
        model = Tipo_Plato
        fields = ["nombre"]


class DateInput(forms.DateInput):
    input_type = 'date'


class TimePickerInput(forms.TimeInput):
    input_type = 'time'


class PedidoForm(ModelForm):
    class Meta:
        model = Pedido
        fields = ('cliente', 'fechaPedido', 'horaEntrega', 'TipoEntrega', 'Estado', 'menu',
                  'repartidor')

        widgets = {
            'fechaPedido': DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
            'horaEntrega': TimePickerInput(),

        }

    def __init__(self, *args, **kwargs):
        super(PedidoForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class PedidoClienteForm(ModelForm):
    class Meta:
        model = Pedido
        exclude = ('fechaPedido', 'horaEntrega' 'Estado',
                   'repartidor', 'cliente', 'menu', 'comentario')

    def __init__(self, *args, **kwargs):
        super(PedidoClienteForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class ActualizarPedidoForm(forms.Form):
    ESTADO_OPCIONES = (
        (1, "Pendiente"), (2, "En preparación"), (3,
                                                  "En camino"), (4, "Entregado"), (5, "Devuelto"),
        (6, "Cancelado"))

    estadoPedido = forms.ChoiceField(choices=ESTADO_OPCIONES, label=False)
    comentario = forms.CharField(max_length=500, label=False)

# class RepartidorForm(forms.Form):
#     class Meta:
#         model = Repartidor

#         fields = ('CUIT', 'apellido', 'nombre', 'celular', 'telefono_fijo', 'fecha_nacimiento', 'calle', 'numero', 'localidad', 'departamento', 'observaciones', 'fecha_ingreso_trabajar', 'vigencia_carnet', 'patente', 'zona')


class DateRangeForm(forms.Form):
    Fecha_desde = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date'}))
    Fecha_hasta = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kwargs):
        super(DateRangeForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.widget.attrs['type'] = 'date'


class DateRangeCadeteForm(forms.Form):
    Fecha_desde = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date'}))
    Fecha_hasta = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date'}))
    Cadete = forms.ModelChoiceField(queryset=Repartidor.objects.all())

    def __init__(self, *args, **kwargs):
        super(DateRangeCadeteForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.widget.attrs['type'] = 'date'

from .models import Tipo_Menu, Tipo_Plato


def extras(request):
    lista_tipos = Tipo_Menu.objects.all().order_by('nombre')
    return {'tipo_de_menues': lista_tipos}


def extras2(request):
    lista_tipo_platos = Tipo_Plato.objects.all().order_by('nombre')
    return {'platos': lista_tipo_platos}


def importe_total_carro(request):
    total = 0
    if request.user.is_authenticated:
        if "carro" in request.session:
            for key, value in request.session["carro"].items():
                total = total + int(value["precio"])
    return {"importe_total_carro": total}

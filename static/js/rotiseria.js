const botonAdministrativo = document.getElementById("id_tipo_empleado");

const botonRepartidor = document.getElementById("boton_repartidor");

const contenedorCadete = document.getElementById("contenedor_cadete");

botonAdministrativo.addEventListener("click", ocultarDatosCadete);

botonRepartidor.addEventListener("click", mostrarDatosCadete);

function ocultarDatosCadete() {
  document.getElementById("contenedor_cadete").style.display = "none";
}

function mostrarDatosCadete() {
  document.getElementById("contenedor_cadete").style.display = "block";
}
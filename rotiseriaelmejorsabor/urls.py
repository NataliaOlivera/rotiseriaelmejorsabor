"""rotiseriaelmejorsabor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include

from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='base/index.html'), name='inicio'),
    path('inicio_admin/', TemplateView.as_view(template_name='base/index_admin.html'),
         name='inicio_admin'),
    path('persona/', include('apps.persona.urls', namespace='persona')),
    path('persona/cliente', include('apps.persona.urls', namespace='cliente')),
    path('persona/empleado', include('apps.persona.urls', namespace='empleado')),


    path('menu/', include('apps.pedido.urls', namespace='menu')),
    path('menu/compra/', include('apps.pedido.urls', namespace='compra')),
    path('pedido/', include('apps.pedido.urls', namespace='pedido')),

    path('usuario/', include('apps.usuario.urls', namespace='usuario')),

    path('accounts/', include('django.contrib.auth.urls')),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
